var mapStyle = GOOGLE_MAP_STYLES;

var longreadMap;
var longreadMapInfra;

longreadMapInfra = new GMaps({
    div: '.longread-mapInfra',
    lat: 55.557442,
    lng: 37.558758,
    zoom: 15,
    styles: mapStyle
});

longreadMapInfra.addMarker({
    lat: 55.557442,
    lng: 37.558758,
    icon: '/img/location/icon_infra/map_icon_01.png',
    details: {
        type: 1
    }
});

$(".longread4-mapPoints__edu a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: {
            url: "/img/location/icon_infra/univercity.png",
        },
        details: {
            type: 'edu'
        },
    });
});

$(".longread4-mapPoints__kindergarten a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/location/icon_infra/kids.png",
        details: {
            type: 'kindergarten'
        }
    });
});

$(".longread4-mapPoints__cafe a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/location/icon_infra/cafe.png",
        details: {
            type: 'cafe'
        }
    });
});

$(".longread4-mapPoints__shops a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/location/icon_infra/shop.png",
        details: {
            type: 'shops'
        }
    });
});

$(".longread4-mapPoints__med a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/location/icon_infra/medicine.png",
        details: {
            type: 'med'
        }
    });
});

$(".longread4-mapPoints__park a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/location/icon_infra/park.png",
        details: {
            type: 'park'
        }
    });
});

$(".longread4-mapPoints__sport a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/location/icon_infra/sport.png",
        details: {
            type: 'sport'
        }
    });
});

$(".longread4-mapPoints__museums a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/location/icon_infra/museums.png",
        details: {
            type: 'museums'
        }
    });
});


$(".longread4-mapPoints__school a").each(function(index, el) {
    let $link = $(el);
    let lat = $link.data("lat");
    let lng = $link.data("lng");

    longreadMapInfra.addMarker({
        lat: lat,
        lng: lng,
        title: $link.html(),
        icon: "/img/location/icon_infra/school.png",
        details: {
            type: 'school'
        }
    });
});



var insertedMarkers = longreadMapInfra.markers;

$('.longread4-category-links__item').click(function(e) {
    e.preventDefault();
    let category = $(this).data('category');
    let checkHidden = $(this).hasClass('longread4-category-links__item_off');
    if (checkHidden) {
        for (var i = insertedMarkers.length - 1; i >= 0; i--) {
            if (insertedMarkers[i].details.type === category) {
                insertedMarkers[i].setVisible(true);
            }
        }
    } else {
        for (var i = insertedMarkers.length - 1; i >= 0; i--) {
            if (insertedMarkers[i].details.type === category) {
                insertedMarkers[i].setVisible(false);
            }
        }
    }
    $(this).toggleClass('longread4-category-links__item_off');
});

$('.longread4-category-links__show').click(function(e) {
    e.preventDefault();
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        insertedMarkers[i].setVisible(true);
    };
    $('.longread4-category-links__item').removeClass('longread4-category-links__item_off');
});

$('.longread4-category-links__hide').click(function(e) {
    e.preventDefault();
    for (var i = insertedMarkers.length - 1; i >= 0; i--) {
        if (insertedMarkers[i].details.type === 1) {
            insertedMarkers[i].setVisible(true);
        } else {
            insertedMarkers[i].setVisible(false);
        }
    }
    $('.longread4-category-links__item').addClass('longread4-category-links__item_off');
});

longreadMapInfra.fitZoom();

$('.longread-mapInfra-btns__infra').click(function () {
    $('.longread4-category-links').removeClass('longread4-category-links_hidden')
    $('.longread4-category-links').addClass('longread4-category-links_visible')
})

$('.longread-mapInfra-btns__map').click(function () {
    $('.longread4-category-links').removeClass('longread4-category-links_visible')
    $('.longread4-category-links').addClass('longread4-category-links_hidden')
})


function zoomControlToRight() {
    longreadMapInfra.setOptions({ zoomControlOptions: { position: google.maps.ControlPosition.TOP_RIGHT } });
}

zoomControlToRight();

longreadMap = new GMaps({
    div: '.longread-map1',
    lat: 55.882315,
    lng: 37.684119,
    zoom: 14,
    styles: mapStyle
});

longreadMap.addMarker({
    lat: 55.557546,
    lng: 37.558800,
    icon: '/img/location/icon_infra/map_icon_01.png',
});

longreadMap.setCenter({
    lat: 55.568046,
    lng: 37.568800,
});

$('[data-location-tabs]').each(function() {
    let $wrapper = $(this);
    let $tabs    = $wrapper.find('>div');
    let active   = '_active';
    let pos      = $tabs.filter('._active').data('markers');

    $.each(pos, function(i, e) {
        let lat = e.position[0];
        let lng = e.position[1];

        longreadMap.drawRoute({
            origin: [lat, lng],
            destination: [55.557546, 37.558800],
            travelMode: 'walking',
            strokeColor: '#cd1027',
            strokeOpacity: 1,
            strokeWeight: 3
        });
    });

    $tabs.on('click', function() {
        let $btn  = $(this);
        let index = $btn.index();
        let data  = $btn.data('markers');

        if ($btn.hasClass(active)) return;

        $tabs.removeClass(active);
        $btn.addClass(active);

        $('.longread-tabs-descr p').hide().eq(index).show();

        longreadMap.cleanRoute();

        $.each(data, function(i, e) {
            let lat = e.position[0];
            let lng = e.position[1];

            longreadMap.drawRoute({
                origin: [lat, lng],
                destination: [55.557546, 37.558800],
                travelMode: 'walking',
                strokeColor: '#cd1027',
                strokeOpacity: 1,
                strokeWeight: 3
            });
        });

        if (index === 0) {
            longreadMap.setCenter({
                lat: 55.568046,
                lng: 37.568800,
            });
        } else {
            longreadMap.setCenter({
                lat: 55.558046,
                lng: 37.562800,
            });
        }
    });
});
