for (let i = 0; i < 4; i++) {
    let slNum = ".apparts__plansImgSL" + i;
    let slPagNum = ".apparts__plansBull" + i;

    let appartsPlans = new Swiper ( slNum, {
        pagination: {
            el: slPagNum,
            clickable: true
        },
    });
}

$('.swiper-pagination-bullet').hover(function() {
    $( this ).trigger( "click" );
});

{
    let buttonOnFlatsReq = $('[data-text-flat]');
    if (buttonOnFlatsReq.length) {
        buttonOnFlatsReq.on('click', function () {
            let currentText = $(this).data('text-flat');
            $('.page-sidebar__tlt').find('span').text(currentText);
            $('[data-name-flat]').val(currentText);
        });
    }

}