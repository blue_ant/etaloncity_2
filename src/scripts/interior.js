$(function() {
    $('[data-interior-gallery]').each(function() {
        let $gallery = $(this);
        let gallery = new Swiper($gallery, {
            arrows: true,
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            }
        });
    });

    $('[data-interior-panorams]').each(function() {
        let $this = $(this);

        $this.fancybox({
            type: 'iframe',
            baseClass: 'panorams',
            hideScrollbar: true,
        });
    });
});
