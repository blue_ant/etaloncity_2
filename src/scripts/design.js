$(function() {
    $('[data-slider]').each(function() {
        let $this = $(this);
        let $next = $this.find('.swiper-button-next');
        let $prev = $this.find('.swiper-button-prev');
        let type  = $this.data('slider');
        let options;

        if (type === 'simple') {
            options = {
                arrows: true,
                pagination: false,
                navigation: {
                    nextEl: $next,
                    prevEl: $prev,
                },
                loop: true
            }
        } else if (type === 'full') {
            options = {
                arrows: true,
                pagination: {
                    el: '.swiper-pagination',
                    type: 'bullets',
                    clickable: true,
                },
                navigation: {
                    nextEl: $next,
                    prevEl: $prev,
                },
                loop: true
            }

            let colWidth = $this.parent().width();
            let $window  = $(window);

            function sliderWidth() {
                let winWidth = $window.width();
                let count;
                let containerWidth = $('.design .container').width();

                if (winWidth < 768) {
                    $this.removeAttr('style');

                    return;
                }

                count = (winWidth - containerWidth) / 2

                $this.css({
                    width: 'calc(100% + ' + count + 'px'
                });
            }

            sliderWidth();

            $window.on('resize', sliderWidth);
        }

        let gallery = new Swiper($this, options);

        if (type === 'full') {
            gallery.on('slideChange', function () {
                $('.swiper-fraction').html(
                    `<span>` + (Number(gallery.realIndex) + 1) + `</span> / <span>` + (gallery.slides.length - 2) + `</span>`
                );
            });

            console.log(gallery.slides);
        }
    });
});
