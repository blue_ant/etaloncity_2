<% if(data.total !== 0) {%>

<div class="parking-table">
    <table>
        <% _.forEach(data.parkingItems, function(item) { %>

        <tr>
            <td><%= item.num %></td>
            <td><%= item.house %></td>
            <td><%= item.floor %> этаж</td>
            <td><%= item.price.toLocaleString("ru-RU") %> руб</td>
            <td><a href="<%= item.plan %>" class="download <%= item.plan === '' ? 'download_disabled' : '' %>" target="_blank"><span>загрузить</span></a></td>
        </tr>

        <% }); %>

    </table>
</div>



<%}else{%>
<div class="NoPlansFound" style="text-align:center;padding:40px;"><p class="NoPlansFound_text">Квартиры по заданным параметрам не найдены,<br class="hidden-xs"> пожалуйста измените параметры поиска.</p></div>
<% }; %>