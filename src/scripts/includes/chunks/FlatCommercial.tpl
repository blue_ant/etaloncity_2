<% if(data.total !== 0) {%>

<div class="Tbl Tbl-commercial" id="listViewResult">
    <% _.forEach(data.flatsCommercial, function(flt) { %>

    <a href="<%= flt.href %>" class="Tbl_tr Tbl_tr-commercialFlat">
        <div class="Tbl_td Tbl_td-house">
            <span class="hidden-xs">Корпус <%= flt.house %> / секция <%= flt.section %></span>
            <span class="hidden-xxl hidden-xl hidden-lg hidden-md hidden-sm">К <%= flt.house %>/сек<%= flt.section %> <br> <%= flt.floor %> этаж</span>
        </div>
        <div class="Tbl_td Tbl_td-type"><%= flt.type %></div>
        <div class="Tbl_td Tbl_td-floor"><%= flt.floor %> этаж</div>
        <div class="Tbl_td Tbl_td-area"><%= flt.area %> м<sup>2</sup></div>
        <div class="Tbl_td Tbl_td-priceMeter"><%= flt.priceMeter.toLocaleString("ru-RU") %> руб</div>
        <div class="Tbl_td Tbl_td-price">
            <span class="hidden-xs"><%= flt.price.toLocaleString("ru-RU") %> руб</span>
            <span class="hidden-xxl hidden-xl hidden-lg hidden-md hidden-sm">
            <%= flt.priceMeter.toLocaleString("ru-RU") %> р.<br>
            <%= flt.price.toLocaleString("ru-RU") %> р.
            </span>
        </div>
    </a>

    <% }); %>

</div>

<%}else{%>
<div class="NoPlansFound" style="text-align:center;padding:40px;"><p class="NoPlansFound_text">Квартиры по заданным параметрам не найдены,<br class="hidden-xs"> пожалуйста измените параметры поиска.</p></div>
<% }; %>