class RangeSlider {
    constructor(el) {
        if (!el) {
            console.error("RangeSlider constructor can\'t find given \"el\" in DOM!");
            return;
        }

        let $el = $(el);

        let $inputs = $el.find('input');
        let $inpMin = $inputs.eq(0);
        let $inpMax = $inputs.eq(1);

        let inlineElOpts = $el.data('slider-opts');
        let counterType = inlineElOpts.counterType || "num";
        let minVal = inlineElOpts.min;
        let maxVal = inlineElOpts.max;

        let currentMinVal = parseInt($inpMin.val());
        let currentMaxVal = parseInt($inpMax.val());

        let $minValCounter = $el.find('.RangeSlider_minValViewEl');
        let $maxValCounter = $el.find('.RangeSlider_maxValViewEl');

        

        if (counterType === "date_quarters") {
            this.quarterPoints = this._getQuartersFromRange(minVal, maxVal);
            minVal = 0;
            maxVal = this.quarterPoints.length - 1;

            let currentValuesQuaters = this._getQuartersFromRange(currentMinVal, currentMaxVal);

            currentMinVal = _.findIndex(this.quarterPoints, (t) => {
                return currentValuesQuaters[0].utc === t.utc;
            });

            currentMaxVal = _.findIndex(this.quarterPoints, (t) => {
                return _.last(currentValuesQuaters).utc === t.utc;
            });
        }

        let options = {
            classes: {
                "ui-slider-handle": "RangeSlider_handle",
                "ui-slider-range": 'RangeSlider_progress'
            },
            slide: (event, ui) => {
                this.renderValuesToCounterEl(ui.values);
            },
            change: (event, ui) => {
                this.setValuesToInputs(ui.values[0], ui.values[1]);
            },
            values: [currentMinVal, currentMaxVal],
            min: minVal,
            max: maxVal,
            step: inlineElOpts.step || 1,
            range: true,
        };

        let $track = $("<div class='RangeSlider_track'></div>");
        $track.slider(options).prependTo($el);

        this.$minValCounter = $minValCounter;
        this.$maxValCounter = $maxValCounter;

        this.$inpMin = $inpMin;
        this.$inpMax = $inpMax;

        this.minMaxValues = [minVal, maxVal];

        this.counterType = counterType;

        this.$track = $track;

        this.renderValuesToCounterEl($track.slider("values"));
    }

    setValuesToInputs(min, max) {
        if (this.counterType === "date_quarters") {
            min = this.quarterPoints[min].utc;
            max = this.quarterPoints[max].utc;
        }
        this.$inpMin.val(min);
        this.$inpMax.val(max).trigger('change');
    }

    _getQuartersFromRange(start, end) {
        let startDate = new Date(start * 1000);
        let endDate = new Date(end * 1000);

        let result = [];

        let quarterPoints = [moment(startDate).unix()];
        let tmpQuarterPoint = startDate;

        while (tmpQuarterPoint < endDate) {
            tmpQuarterPoint.setMonth(tmpQuarterPoint.getMonth() + 3);
            quarterPoints.push(moment(tmpQuarterPoint).unix());
        }

        let fmtTpl = 'Q кв. YYYY';

        let titles = [];

        for (let i = 0; i < quarterPoints.length; i++) {

            let romanNumbers = [
                "I",
                "II",
                "III",
                "IV",
            ];

            let titleStr = moment.unix(quarterPoints[i]).format(fmtTpl);
            let quartIndex = parseInt(titleStr) - 1;
            titleStr = romanNumbers[quartIndex] + titleStr.slice(1, titleStr.lenght);
            titles.push(titleStr);
        }

        for (var i = 0; i < titles.length; i++) {
            result.push({
                utc: quarterPoints[i],
                title: titles[i],
            });

        }
        return result;
    }

    

    renderValuesToCounterEl(vals) {
        if (this.counterType) {
            switch (this.counterType) {
                case "formatted_num":

                    vals = _.mapValues(vals, (item) => {
                        return item.toLocaleString("ru-RU");
                    });

                    break;

                case "date_quarters":
                    vals =  _.mapValues(vals, (item) => {
                        return this.quarterPoints[item].title;
                    });
                    break;
            }
        }

        window.requestAnimationFrame(() => {
            this.$minValCounter.html(vals[0]);
            this.$maxValCounter.html(vals[1]);
        });
    }


    reset() {
        this.$track.slider('values', this.minMaxValues);
        this.renderValuesToCounterEl(this.minMaxValues);
    }
}