$('.news-head__year select').selectmenu({
    appendTo: ".news-head__year",
    select: function( event, ui ) { window.location.href = ui.item.value }
});


$('.news__title').click(function () {
    let url = window.location.href;
    localStorage.setItem('UrlNewsPage', url);
});

$('.news-detail__back').click(function (e) {
    e.preventDefault();
    let url = localStorage.getItem('UrlNewsPage');
    window.location = url;
});