var loadImage = function(src, callback) {
    var img = new Image();
    img.onload = img.onerror = function(evt) {
        callback(evt.type == "error", img);
        img.onload = img.onerror = img = null;
    };
    img.src = src;
};

let $numbers = $(`.genplan__number`);

$(".genplan__object").hover(
    function(e) {
        var pos = $(".genplan__content").offset();
        var elem_left = pos.left;
        var elem_top = pos.top;
        var Xinner = e.pageX - elem_left;
        var Yinner = e.pageY - elem_top;

        $(`.genplan__render[data-id=${this.dataset.id}]`).addClass("genplan__render--active");
        $numbers.not(`[data-id=${this.dataset.id}]`).addClass("genplan__number--pale");
        // $(`.genplan__number[data-id=${this.dataset.id}]`).addClass("genplan__number--active");
        $(`.genplan__label[data-id=${this.dataset.id}]`)
            .css({ top: Yinner + 10, left: Xinner })
            .addClass("genplan__label--active");
    },
    function() {
        $(`.genplan__render[data-id=${this.dataset.id}]`).removeClass("genplan__render--active");
        $numbers.removeClass("genplan__number--pale");
        $(`.genplan__label[data-id=${this.dataset.id}]`).removeClass("genplan__label--active");
    }
);

$(".genplan__object").click((event) => {
    window.location = $(event.currentTarget).data("bld-url");
});

$numbers
    .hover(
        function(event) {
            event.preventDefault();
            $(`.genplan__object[data-id='${$(this).data("id")}']`).trigger("mouseenter");
            return false;
        },
        function(event) {
            event.preventDefault();
            $(`.genplan__object[data-id='${$(this).data("id")}']`).trigger("mouseleave");
            return false;
        }
    )
    .on("click", function(event) {
        event.preventDefault();
        $(`.genplan__object[data-id='${$(this).data("id")}']`).trigger("click");
    });

loadImage($(".genplan__img-common").attr("src"), function(err, img) {
    if (err) {
        alert("image not load");
    } else {
        var ratio = img.width / img.height;
        var windowWidth = $(window).width();

        updateGenplan(ratio, windowWidth);

        $(window).resize(() => {
            windowWidth = $(window).width();
            updateGenplan(ratio, windowWidth);
        });
    }
});

function updateGenplan(ratio, windowWidth) {
    resizeImg(ratio);
    let $genplan = $(".genplan");
    let $genplanContent = $genplan.find(".genplan__content");
    if (windowWidth >= 1280) {
        genplanPosition();
        $genplan.css({ "overflow-x": "" });
    } else {
        $genplanContent.css({ top: "", left: "" });
        $genplan.css({ "overflow-x": "auto" });

        $genplan.scrollLeft(($genplanContent.width() - $(window).width()) / 2);
    }
    $genplan.css("visibility", "visible");
}

function resizeImg(ratio) {
    var genWidth = $(".genplan").outerWidth();
    var genHeight = $(".genplan").outerHeight();
    var getRatio = genWidth / genHeight;

    if (getRatio < ratio) {
        $(".genplan__content").css({ width: `${genHeight * ratio}px`, height: "100%" });
        $(".genplan__img-common").css({ width: `${genHeight * ratio}px`, height: "100%" });
    } else {
        $(".genplan__content").css({ width: "", height: "" });
        $(".genplan__img-common").css({ width: "", height: "" });
    }
}

function genplanPosition() {
    let $genplan = $(".genplan");
    let $genplanContent = $genplan.find(".genplan__content");

    var genWidth = $genplan.outerWidth();
    var genHeight = $genplan.outerHeight();

    var genContentWidth = $genplanContent.outerWidth();
    var genContentHeight = $genplanContent.outerHeight();

    $genplanContent.css({
        top: `${-(genContentHeight - genHeight) / 2}px`,
        left: `${-(genContentWidth - genWidth) / 2}px`,
    });
}

$('[id^="p-point"], .p-point').each(function(i, el) {
    var $point = $(el);
    var camNum = $point.data('cam');

    $point.on('click', function() {
        var element;

        if (camNum === 'baloon') {
            element =  $('<iframe style="width: 100%; height: 100%; margin: 0; padding: 0; overflow: hidden;" src="https://etaloncity.ru/panorama.html"></iframe>');
        } else {
            element = $('<div id="panorama-' + camNum + '" class="panorama-view"></div>');
        }

        $.fancybox.open(element, {
            touch: false,
            type: 'html',
            arrows: false,
            afterShow: function(i, cur) {
                var height = $(window).height() - 50;
                var width  = $(window).width() - 50;

                if (camNum === 'baloon') {
                    $(cur).css({
                        width: width,
                        height: height
                    });

                    $('body').addClass('panorama-open');

                    return;
                }

                $(cur).find('.panorama-view').css({
                    width: width,
                    height: height
                });

                pannellum.viewer('panorama-' + camNum, {
                    "default": {
                        "firstScene": "scene" + camNum,
                        "type": "equirectangular",
                        "autoLoad": true,
                        "sceneFadeDuration": 1000
                    },
                    "scenes": {
                        "scene1": {
                            "panorama": '/img/genplan/panorams/Cam-01.jpg',
                            "hotSpots": [{
                                "pitch": -6.75,
                                "yaw": 1.00,
                                "type": "scene",
                                "text": "Камера 2",
                                "sceneId": "scene2"
                            }, {
                                "pitch": -4.3,
                                "yaw": -21,
                                "type": "scene",
                                "text": "Камера 5",
                                "sceneId": "scene5"
                            }]
                        },
                        "scene2": {
                            "panorama": '/img/genplan/panorams/Cam-02.jpg',
                            "hotSpots": [{
                                "pitch": -6.75,
                                "yaw": 1.00,
                                "type": "scene",
                                "text": "Камера 1",
                                "sceneId": "scene1"
                            }, {
                                "pitch": -16,
                                "yaw": 96,
                                "type": "scene",
                                "text": "Камера 5",
                                "sceneId": "scene5"
                            }, {
                                "pitch": -5,
                                "yaw": 44,
                                "type": "scene",
                                "text": "Камера 4",
                                "sceneId": "scene4"
                            }]
                        },
                        "scene3": {
                            "panorama": '/img/genplan/panorams/Cam-03.jpg',
                            "hotSpots": [{
                                "pitch": -5,
                                "yaw": 28,
                                "type": "scene",
                                "text": "Камера 4",
                                "sceneId": "scene4"
                            }]
                        },
                        "scene4": {
                            "panorama": '/img/genplan/panorams/Cam-04.jpg',
                            "hotSpots": [{
                                "pitch": -7,
                                "yaw": 120,
                                "type": "scene",
                                "text": "Камера 3",
                                "sceneId": "scene3"
                            }, {
                                "pitch": -5,
                                "yaw": -55,
                                "type": "scene",
                                "text": "Камера 5",
                                "sceneId": "scene5"
                            }]
                        },
                        "scene5": {
                            "panorama": '/img/genplan/panorams/Cam-05.jpg',
                            "hotSpots": [{
                                "pitch": -3,
                                "yaw": -31,
                                "type": "scene",
                                "text": "Камера 1",
                                "sceneId": "scene1"
                            }, {
                                "pitch": -12,
                                "yaw": -85,
                                "type": "scene",
                                "text": "Камера 2",
                                "sceneId": "scene2"
                            }, {
                                "pitch": -6,
                                "yaw": 13,
                                "type": "scene",
                                "text": "Камера 4",
                                "sceneId": "scene4"
                            }]
                        },
                        "scene6": {
                            "panorama": '/img/genplan/panorams/Cam-06.jpg'
                        },
                        "scene21": {
                            "panorama": '/img/genplan/panorams/html5/0.jpg',
                            "hotSpots": [{
                                "pitch": 19.19,
                                "yaw": -11,
                                "type": "scene",
                                "text": "Обычные панорамы",
                                "sceneId": "scene1"
                            }]
                        },
                        "scene22": {
                            "panorama": '/img/genplan/panorams/html5/1.jpg',
                            "hotSpots": [{
                                "pitch": 19.19,
                                "yaw": -11,
                                "type": "scene",
                                "text": "Обычные панорамы",
                                "sceneId": "scene2"
                            }]
                        },
                        "scene23": {
                            "panorama": '/img/genplan/panorams/html5/2.jpg',
                            "hotSpots": [{
                                "pitch": 19.19,
                                "yaw": -11,
                                "type": "scene",
                                "text": "Обычные панорамы",
                                "sceneId": "scene3"
                            }]
                        },
                        "scene24": {
                            "panorama": '/img/genplan/panorams/html5/3.jpg',
                            "hotSpots": [{
                                "pitch": 19.19,
                                "yaw": -11,
                                "type": "scene",
                                "text": "Обычные панорамы",
                                "sceneId": "scene4"
                            }]
                        },
                        "scene25": {
                            "panorama": '/img/genplan/panorams/html5/4.jpg',
                            "hotSpots": [{
                                "pitch": 19.19,
                                "yaw": -11,
                                "type": "scene",
                                "text": "Обычные панорамы",
                                "sceneId": "scene5"
                            }]
                        },
                        "scene26": {
                            "panorama": '/img/genplan/panorams/html5/5.jpg',
                            "hotSpots": [{
                                "pitch": 19.19,
                                "yaw": -11,
                                "type": "scene",
                                "text": "Обычные панорамы",
                                "sceneId": "scene6"
                            }]
                        }
                    }
                });
            },
            afterClose: function() {
                $('body').removeClass('panorama-open');
            },
            btnTpl: {
                close: '<button asd data-fancybox-close class="fancybox-close-small" title="Close"></button>'
            }
        });
    });
});
