function hoverGalleryAlbum() {
    var winWidth = $(window).width();
    if (winWidth > 1023) {
        $(".gallery-grid-item__link").hover(
            function() {
                $(this)
                    .parent()
                    .find(".gallery-grid-item-descr")
                    .addClass("gallery-grid-item-descr_hover");
                $(this)
                    .parent()
                    .find(".gallery-grid-item__info")
                    .addClass("gallery-grid-item__info_disable");
            },
            function() {
                $(this)
                    .parent()
                    .find(".gallery-grid-item-descr")
                    .removeClass("gallery-grid-item-descr_hover");
                $(this)
                    .parent()
                    .find(".gallery-grid-item__info")
                    .removeClass("gallery-grid-item__info_disable");
            }
        );
    } else {
        $(".gallery-grid-item__link").hover(
            function() {
                $(this)
                    .parent()
                    .find(".gallery-grid-item__info")
                    .removeClass("gallery-grid-item__info_disable");
            },
            function() {
                $(this)
                    .parent()
                    .find(".gallery-grid-item__info")
                    .removeClass("gallery-grid-item__info_disable");
            }
        );
    }
}

var sliderGallery = new Swiper(".gallery-slider", {
    speed: 800,
    spaceBetween: 0,
    observer: true,
    observeParents: true,
    slidesPerView: "auto",
    centeredSlides: true,
    preloadImages: false,
    lazy: {
        loadPrevNext: true,
        loadPrevNextAmount: 2
    },

    navigation: {
        nextEl: ".gallery-slider__next",
        prevEl: ".gallery-slider__prev",
    },

    pagination: {
        el: ".gallery-slider-fraction",
        type: "fraction",
    },
    on: {
        slideChange: _.debounce(function() {
            var realIndex = this.realIndex;
            window.location.hash = "slide_" + realIndex;
            $(sliderGalleryThumb.slides).removeClass("is-selected");
            $(sliderGalleryThumb.slides)
                .eq(realIndex)
                .addClass("is-selected");
            sliderGalleryThumb.slideTo(realIndex, 800, false);
        }, 100),
    },
});

var sliderGalleryThumb = new Swiper(".gallery-slider-thumb", {
    slidesPerView: "auto",
    spaceBetween: 12,
    slideToClickedSlide: true,
    observer: true,
    observeParents: true,

    on: {
        click: function() {
            var clicked = this.clickedIndex;
            $(this.slides).removeClass("is-selected");
            $(this.clickedSlide).addClass("is-selected");
            sliderGallery.slideToLoop(clicked, 800, false);
        },
    },

    breakpoints: {
        767: {
            spaceBetween: 10,
        },
    },
});

$('.gallery-grid-item__link').click(function (e) {
    e.preventDefault();

    let album = $(this).data('album-id');
    let slideAlbum = $('[data-hash="'+album+'"]');
    let indexSlideAlbum = slideAlbum.index();

    window.location.hash = "slide_"+indexSlideAlbum
});

let isModalOpen = false;

let $galleryModal = $("#galleryModal");

function handleHash() {
    let slideIndex = window.location.hash.split("slide_")[1];

    if (slideIndex) {
        if (isModalOpen) {
            sliderGalleryThumb.slideTo(slideIndex, 800, false);
            sliderGallery.slideToLoop(slideIndex, 800, false);
        } else {
            sliderGalleryThumb.slideTo(slideIndex, 0, false);
            sliderGallery.slideToLoop(slideIndex, 0, false);
            $.fancybox.open($galleryModal, {
                modal: true,
                touch: false,
                afterShow: () => {
                    isModalOpen = true;
                },
                afterClose: () => {
                    isModalOpen = false;
                    window.location.hash = "";
                },
            });
        }
    } else {
        $.fancybox.close();
    }
}

window.addEventListener("hashchange", handleHash);

handleHash();
hoverGalleryAlbum();
