const FLATS_PER_PAGE = 24;

new ViewSwitch(".ViewSwitch", {
    /*onChange: ($activeBtn) => {
        console.log($activeBtn.attr('href'));
    }*/
});

let cardTplStr = `
/*=require ./includes/chunks/FlatCard.tpl */
`;

let flatsListBuilder = new FlatsList({
    tpl: cardTplStr,
    $mountEl: $("#filterResult"),
});

let $pager = $(".PickupFlatLayout_footerPaginationWrap").pagination({
    itemsOnPage: FLATS_PER_PAGE,
    prevText: "&nbsp;",
    nextText: "&nbsp;",
    displayedPages: 3,
    ellipsePageSet: false,
    edges: 0,
    onPageClick: (num) => {
        filter.offset = (num - 1) * FLATS_PER_PAGE;
        console.log("pager click!!!");
        filter.$filterForm.trigger("submit");
        $("html,body").animate({ scrollTop: sorter.$el.parent().offset().top }, 700, "easeInOutExpo");
        return false;
    },
});

let $progress = $(".PickupFlatLayout_progress");
let $resultsCont = $(".PickupFlatLayout_resultsCont");

let filter = new FilterForm("#filterForm", {
    submitHandler: ($filterForm) => {
        // console.log(" --- submitHandler fired---", "filter.offset: ", filter.offset);

        $resultsCont.addClass("PickupFlatLayout_resultsCont-loading");
        $progress.show().animate({ width: "33%", opacity: 1 }, 600);

        $.ajax({
            url: $filterForm.attr("target"),
            dataType: "json",
            method: $filterForm.attr("method"),
            async: true,
            data: $.extend(true, $filterForm.serializeObject(), {
                action: "get_flats",
                limit: FLATS_PER_PAGE,
                offset: filter.offset,
            }),
            xhr: () => {
                let xhr = new window.XMLHttpRequest();
                xhr.addEventListener("progress", (event) => {
                    if (event.lengthComputable) {
                        let percent = Math.ceil((100 * event.loaded) / event.total);
                        $progress.stop(true, false).animate({ width: percent + "%" }, 400);
                        if (percent === 100) {
                            $progress.animate({ opacity: 0 }, 350, () => {
                                $progress.removeAttr("style");
                            });
                        }
                    }
                });

                return xhr;
            },
        })
            .done((jsonResponse) => {
                // fix pics urls in development enviroment
                if (window.location.href.indexOf("/filters.html") != -1) {
                    console.warn("rewrite json response for dev enviroment usage...");
                    jsonResponse.flats.forEach((flt) => {
                        flt.pic = "https://etalonsad.ru/" + flt.pic;
                        flt.href = "/flat.html";
                    });
                    console.log('jsonResponse', jsonResponse)
                }

                {
                    let promos = jsonResponse.promos;
                    if (promos && promos.length) {
                        promos.forEach((promo)=>{
                            jsonResponse.flats.splice(promo.order-1, 0, promo);
                        });
                    }
                }

                // render plans
                $("#filteredFlatsCounter").html(jsonResponse.total);

                flatsListBuilder.render({
                    data: jsonResponse,
                });

                $pager.pagination("updateItems", jsonResponse.total);

                {
                    let currentPage = filter.offset / FLATS_PER_PAGE + 1;
                    $pager.pagination("drawPage", currentPage);
                }

                $resultsCont.removeClass("PickupFlatLayout_resultsCont-loading");
            })
            .fail(() => {
                alert("Не удалось получить данные с сервера!\nПопробуйте позже.");
            });
    },
});

let sorter = new Sorter(".Sorter");
let sorterTiles = new Sorter(".Sorter-tiles");

$("#filterResult").tooltip({
    items: ".actionIcon",
    classes: {
        "ui-tooltip": "actionIconTooltip",
    },
});
