const $window = $(window);

$(() => {
    $('[data-map]').each(function() {
        let map = new Map($(this));
    });

    $('[data-toggle-list]').each(function() {
        var $wrapper = $(this);
        var $items   = $wrapper.find('.contact-list__item')
        var $titles  = $items.find('.contact-list__tlt');
        var $blocks  = $items.find('.contact-list__cont');

        $titles.on('click', function() {
            let $this  = $(this);
            let $item  = $this.parent();
            let $block = $item.find('.contact-list__cont');

            $blocks.stop().animate({
                height: 0
            });

            if ($item.hasClass(active)) {
                $items.removeClass(active);
            } else {
                $items.removeClass(active);
                $item.addClass(active);
                $block.stop().animate({
                    height: $block.get(0).scrollHeight
                });
            }
        });
    });

    $('.contact-offices-types').tabs({
        classes: {
            "ui-tabs": "contact-offices-types-tabs",
            "ui-tabs-nav": "contact-offices-types-tabs__nav",
            "ui-tabs-tab": "contact-offices-types-tabs__tab",
            "ui-tabs-panel": "contact-offices-types-tabs__panel"
        }
    });

    var winWidth = $window.width();

    if (winWidth < 768) {
        $('.contacts-header-links').appendTo('.contact-offices');
    } else {
        $('.contacts-header-links').appendTo('.contacts-header-inner');
    }

    $window.resize(function() {
        var winWidth = $window.width();

        if (winWidth < 768) {
            $('.contacts-header-links').appendTo('.contact-offices');
        } else {
            $('.contacts-header-links').appendTo('.contacts-header-inner');
        }
    });

    $(".contact-offices-item__text").mCustomScrollbar({
        scrollInertia: 0,
        mouseWheel: { preventDefault: true },
    });
});

function teamScroll() {
    if (window.matchMedia("(max-width: 1023px)").matches) {
        $("[data-team]").mCustomScrollbar('destroy');
    } else {
        $("[data-team]").mCustomScrollbar({
            scrollInertia: 0,
            mouseWheel: { preventDefault: true },
        });
    }
}
function teamScrollXS() {
    if (window.matchMedia("(max-width: 1023px)").matches) {
        $('.contact-offices-types-tabs__nav').css('width', $window.width() + 'px');
    } else {
        $('.contact-offices-types-tabs__nav').css('width', '');
    }
}
teamScroll();
setTimeout(teamScrollXS, 300);
$window.on('resize', function () {
    teamScroll();
    teamScrollXS();
});
