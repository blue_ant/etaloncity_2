var burger = $(".header__burger");

let homeBanner = new Swiper(".home-banner-list", {
    effect: "slide",
    slidesPerView: 1,
    resistanceRatio: 0,
    autoplay: {
        delay: 4200,
        disableOnInteraction: false,
    },
    speed: 1000,
    pagination: {
        el: '.home-banner-list__pagination',
        type: 'bullets',
    }
});

$(".video-about-btn").fancybox({
    /*beforeClose: function(instance, slide) {
        burger.show("fast");
    },*/
    smallBtn: true,
    hideScrollbar: false,
    backFocus: false,
    modal: false,
    buttons: [],
    touch: false,
    baseClass: "video-about-modal",
    btnTpl: {
        smallBtn:
        '<button data-fancybox-close class="close-gallery close-gallery_video" title="{{CLOSE}}">' +
        '<img src="img/close-popup-dark.svg" alt=""></img>' +
        "</button>",},

});

let hideScrollForBannerSlide = () => {
    let activeSlide = $('.home-banner-list').find('.swiper-slide-active');
    let scrollingIcon = $('.home-banner__scrolling');
    let pagination = $('.home-banner-list__pagination');
    let slideIsBanner = activeSlide.hasClass('banner-slide');

    if (slideIsBanner) {
        scrollingIcon.addClass('home-banner__scrolling_hidden');
        pagination.addClass('home-banner-list__pagination_hidden');
    } else {
        scrollingIcon.removeClass('home-banner__scrolling_hidden');
        pagination.removeClass('home-banner-list__pagination_hidden');
    }
}

hideScrollForBannerSlide();

homeBanner.on('slideChangeTransitionEnd', function () {
    console.log('slide change')
    hideScrollForBannerSlide();
});



